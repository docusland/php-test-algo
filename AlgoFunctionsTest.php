<?php

require "AlgoFunctions.php";
use PHPUnit\Framework\TestCase;
class AlgoFunctionsTest extends TestCase
{
    /** Exercice 8.5 dans Module Algo - Functions 
     * Cesar
     */
    public function testCesarDecalageDeUn()
    {
        $this->markTestSkipped('must be revisited.');
        $this->assertEquals('BCDEF', cesar('ABCDE'));
    }

    /** Exercice 8.6 dans Module Algo - Functions 
     * Cesar
     */
    public function testCesarDecalageDeTreize()
    {
        $this->markTestSkipped('must be revisited.');
        $this->assertEquals('NOPQR', cesar('ABCDE', 13));
    }

    /** Exercice 8.6 - bonus dans Module Algo - Functions 
     * Cesar
     */
    public function testCesarChangementCasse()
    {
        $this->markTestSkipped();
        $this->assertEquals('efvhfm', cesar('deluge'));
    }

    /**
     * Exercice 8.7 dans Module Algo - Functions
     */
    public function testArrayMapping()
    {
        $this->markTestSkipped();
        $this->assertEquals('HYLUJ', arrayMapping('ABCDE', 'HYLUJPVREAKBNDOFSQZCWMGITX'));
        $this->assertEquals('SPQR', arrayMapping('FORT', 'AYLUJSVREHKBNDPFOQZRWMGITX'));
        $this->assertEquals('minuscules', arrayMapping('minuscules', 'abcdefghijklmnopqrstuvwxyzé'));
    }

    /** Bonus dans Eval Algo - Functions 
     * Roman to arabic conversion.
     * example: MCMLXXXV -> 1985
     */
    public function testRomanToArabic()
    {
        $this->markTestSkipped();
        $this->assertEquals(1985, romanToArabic('MCMLXXXV'));
    }
}