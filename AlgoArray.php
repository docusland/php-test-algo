<?php
/**
 * Exo 5.1
 */
function arrayFullOfZeroes(int $nb_zeroes = 7) :array {
}

/**
 * Exo 5.5
 */
function sumOfArrays(array $tab_1, array $tab_2) {
}

/**
 * Exo 5.6
 */
function multiplicationOfArrays(array $tab_1, array $tab_2) {
}

/**
 * Exo 5.7
 * Find maximum value and it's position. 
 * Returns an array with structure : 
 * [0] => position
 * [1] => value
 */
function maxValueAndPosition(array $array) : array {

}