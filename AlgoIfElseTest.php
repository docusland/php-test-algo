<?php

require "AlgoIfElse.php";
use PHPUnit\Framework\TestCase;
class AlgoIfElseTest extends TestCase
{

    
    /**
     * Which one is bigger ?
     * Algorithme qui permet d'afficher le plus grand de trois nombre entiers saisis  au clavier
     */
    public function testSameValues()
    {
        $this->assertEquals(2, which_one_is_bigger(2,2,2));
    }

    public function testDifferentValues()
    {
        $this->assertEquals(9, which_one_is_bigger(1,5,9));
    }

    public function testNegativeValues()
    {
        $this->assertEquals(-1, which_one_is_bigger(-1,-5,-9));
    }
    /** 
     * Exercice Bob
     *  Bob est un robot avec une conversation très limitée. Ecrivez sa fonction.
     *  Bob dit 'Ouais.' lorsque vous lui posez une question. Ex : bob("Tu vas bien ?")--> 'Ouais'
     *  Il fait 'Humm' si vous ne lui dites rien. Ex: bob("") --> 'Humm'
     *  Dans tous les autre cas il réponds 'Ok'

     */
    public function testBobSaysHumm()
    {
        $this->markTestSkipped('must be revisited.');
        $this->assertEquals('Humm', bob(''));
    }
    public function testBobSaysOk() {
        $this->markTestSkipped('must be revisited.');
        $this->assertEquals('Ok', bob('Je mange des spaghettis.'));
    }
    public function testBobSaysYes() {
        $this->markTestSkipped('must be revisited.');
        $this->assertEquals('Ouais.', bob('Il fait beau aujourd\'hui ?'));
    }



}