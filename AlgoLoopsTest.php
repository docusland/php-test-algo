<?php
require "AlgoLoops.php";
use PHPUnit\Framework\TestCase;
class AlgoLoopsTest extends TestCase {

    /**
     * Exercice 4.1
     * Fonction qui retourne un booléen.
     * Validant le fait que le paramètre est compris entre 1 et 10.
     * Les valeurs 1 et 10 inclues.
     */
    public function testIsNumberBetweenOneAndTen()
    {
        $this->markTestSkipped();
        $this->assertEquals(false, isNumberBetweenOneAndTen(123));
        $this->assertEquals(false, isNumberBetweenOneAndTen(-123));
        $this->assertEquals(false, isNumberBetweenOneAndTen(0));
        $this->assertEquals(false, isNumberBetweenOneAndTen(11));
        $this->assertEquals(true, isNumberBetweenOneAndTen(3));
        $this->assertEquals(true, isNumberBetweenOneAndTen(1));
        $this->assertEquals(true, isNumberBetweenOneAndTen(10));

    }

    /**
     * Exercice 4.2
     * Fonction qui prends un paramètre, et nous retourne une chaine de caractères. 
     * Contenant une suite de 10 nombres consecutifs à partir du paramètre saisi
     */
    public function testListOfTenNumbers() {
        $this->markTestSkipped();
        $this->assertEquals('1,2,3,4,5,6,7,8,9,10', listOfTenNumbers(1));
        $this->assertEquals('10,11,12,13,14,15,16,17,18,19', listOfTenNumbers(10));
    }

    /**
     * Exercice 4.4
     * Fonction qui retourne, à partir d'un nombre de départ, la
     * table de multiplication de ce nombre, présentée comme suit (cas où l’utilisateur
     * entre le nombre 7) 
     * 7 x 1 = 7
     * 7 x 2 = 14
     * ...
     * 7 x 9 = 63
     */
    public function testMultiplicationTable() {
        $this->markTestSkipped();
        $result = "7 x 1 = 7\n7 x 2 = 14\n7 x 3 = 21\n7 x 4 = 28\n7 x 5 = 35\n7 x 6 = 42\n7 x 7 = 49\n7 x 8 = 56\n7 x 9 = 63\n";
        $this->assertEquals($result, multiplicationTable(7));
    }

    /**
     * Exercice 4.5
     * A partir d'un nombre de départ,  calcule la somme des entiers jusqu’à ce nombre. 
     * Par exemple, si l’on entre 5, le programme doit calculer : 1 + 2 + 3 + 4 + 5 = 15
     */
    public function testSumOfArray() {
        $this->markTestSkipped();
        $this->assertEquals(1, sumOfArray(1));
        $this->assertEquals(3, sumOfArray(2));
        $this->assertEquals(15, sumOfArray(5));
        $this->assertEquals(1275, sumOfArray(50));
    }

    /**
     * Exercice 4.6
     * Fonction qui calcule sa factorielle.
     * NB : la factorielle de 8, notée 8! et vaut 1 x 2 x 3 x 4 x 5 x 6 x 7 x 8 = 40320
     */
    public function testFactorielle() {
        $this->markTestSkipped();
        $this->assertEquals(1, factorielle(1));
        $this->assertEquals(40320, factorielle(8));
    }
    public function testFactorielleParticularity() {
        $this->markTestSkipped();
        $this->assertEquals(1, factorielle(0));
    }

    /**
     * Exercice 4.9
     * Vous connaissez la constante PI ? Connaissez-vous une formule permettant de la définir ? 
     * L’une des premières formules permettant de trouver PI a été la suivante:
     * 𝜋/4 = 1 − 1/3 + 1/5 - 1/7 + 1/9 ...
     * Cette suite infinie permet de se rapprocher de pi petit à petit.
     * Mais les premières valeurs en sont très loin! 
     * 
     * Créez la fonction generatePi, qui nous retourne la nieme valeur pour cette formule. 
     */

    public function testPi() {
        $this->markTestSkipped();
        $this->assertEquals(4, generatePi(0));
        $this->assertEquals(4*(1 - 1/3), generatePi(1));
        $this->assertEquals(4*(1 - 1/3 + 1/5), generatePi(2));
        $this->assertEquals(3.14, number_format(generatePi(900), 2));
    }

}