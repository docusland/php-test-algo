Test Unitaires permettant de s'entrainer en programmation PHP.

Un ensemble de fonctions ont été présentées lors d'exercices d'algorythmique. Ici , grâce à la bibliothèque phpunit, des tests unitaires ont été mis en place permettant de les refaire mais en PHP. 

Les tests unitaires sont des fonctions qui viennent executer d'autres fonctions afin de valider si ces dernières produisent bien le résultat attendu. Permettant donc de tester la fonction avec différents paramètres. 

Afin d'executer les tests unitaires, il sera nécessaire d'installer phpunit
Pour ce faire, suivez les instructions listées ici : https://phpunit.de/index.html

Vous devriez réaliser les instructions suivantes : 
```bash
composer update
```

Puis, il vous suffit de réaliser la commande suivante : 

`./vendor/bin/phpunit <NomDuFichierTest.php>`


Par exemple : 
```
$ ./vendor/bin/phpunit AlgoArrayTest.php 
PHPUnit 9.0.1 by Sebastian Bergmann and contributors.

.                                                                   1 / 1 (100%)

Time: 124 ms, Memory: 10.00 MB

OK (1 test, 1 assertion)
```

Comment commencer ? 
==

Tout simplement, il y a des fichier nommés `Algo<Quelquechose>.php` et d'autres finissant par Test : `Algo<Quelquechose>Test.php`.

Au sein des fichiers Test sont rédigés des tests appelant des fonctions. 
Les fonctions, c'est à vous de les implémenter dans les fichier `Algo<Quelquechose>`.

Certains tests sont des bonus. Commentez la ligne qui les déclare en tant que `skipped` pour les activer.

Vous souhaitez enrichir ce dépot ? 
==
Rien de plus simple. Forkez ce dépot. Puis implémentez vos fonctions et vos tests.
Faites moi des propositions par le biais de merge requests sur la branche improvements. 
Il serait préférable que les fonctions et leurs solutions soient envoyés d'un coup. 
